        <header id="header" class="header shadows">

            <div class="header-menu"> 
                <div class="row">
                    <div class="col-8">
                        <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa-bars"></i></a> 
                        <div class="header-left">
                            <div class="perusahaan"> <div class="text">PT Nusantara Compnet Integrator </div></div>
                        </div>
                    </div> 
                    <div class="col-4">
                        <div class="user-area dropdown float-right">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="user-avatar rounded-circle" src="images/avatar/avatar-big-01.jpg" alt="Admin">
                            </a> 
                            <div class="user-menu dropdown-menu shadows">
                                <a class="nav-link" href="#"><i class="fa fa-user"></i>My Profile</a> 
                                <a class="nav-link" href="index.php"><i class="fa fa-power-off"></i> Logout</a>
                            </div>
                        </div> 

                    </div>
                </div>
            </div>

        </header><!-- /header -->