<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<!-- s:head -->
<title>Yureka - Register</title>
<?php include "_head.php"; ?>
<!-- s:head -->

<body class="bg-dark">

    <div class="d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo"> 
                    <img class="align-content" src="images/logo-eyureka.png" alt="eyureka"> 
                </div>
                <div class="login-form">
                    <form action="berhasil-register.php">
                        <h5 class="form-title">Data Perusahaan</h5>
                        <div class="form-group">
                            <label>Nama Perusahaan</label>
                            <input type="txt" class="form-control" placeholder="Nama Perusahaan">
                        </div>
                        <div class="form-group">
                            <label>NPWP</label>
                            <input type="text" class="form-control" placeholder="NPWP">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="text" id="exampleText" class="form-control"></textarea> 
                        </div> 
                         
                        <h5 class="form-title">Data User</h5>
                        <div class="form-group">
                            <label>Nama User</label>
                            <input type="txt" class="form-control" placeholder="Nama User">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" placeholder="Email">
                        </div> 
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Agree the terms and policy
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat mt-30">Register</button> 
                        <div class="register-link mt-15 text-center">
                            <p>Already have account ? <a href="index.php"> Sign in</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


</body>

</html>
