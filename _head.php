<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="description" content="yureka dashboard">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="images/ico-eyureka.ico" type="image/x-icon">

    <!-- Fontfaces CSS--> 
    <link rel="stylesheet" href="assets/css/font-face.css" media="all">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css"> 
    <link rel="stylesheet" href="vendors/mdi-font/css/material-design-iconic-font.min.css" media="all"> 

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap-extras-margins-padding.css">  
    <link rel="stylesheet" href="assets/css/eyureka.css">  

</head>