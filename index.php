<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<!-- s:head -->
<title>Yureka - Login</title>
<?php include "_head.php"; ?>
<!-- s:head -->

<body class="bg-dark">

    <div class="d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo"> 
                    <img class="align-content" src="images/logo-eyureka.png" alt="eyureka"> 
                </div>
                <div class="login-form">
                    <form action="dashboard.php">
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember Me
                                </label>
                                <label class="pull-right">
                                        forgot your password?<a href="forget-pass.php"> click here</a>
                                </label> 
                        </div>
                        <button type="submit" class="btn btn-success btn-flat mt-30">Sign in</button> 
                        <div class="register-link mt-15">
                            <p>Don't have account ? <a href="register.php"> Sign Up Here</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


</body>

</html>
