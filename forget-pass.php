<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<!-- s:head -->
<title>Yureka - Forget Password</title>
<?php include "_head.php"; ?>
<!-- s:head -->

<body class="bg-dark">

    <div class="d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo"> 
                    <img class="align-content" src="images/logo-eyureka.png" alt="eyureka"> 
                </div>
                <div class="login-form"> 
                    <h5>Forgot Password?</h5> 
                    <div class="mb-20">You can reset your password here.</div>
                    <form action="berhasil-forget.php">
                        <div class="form-group"> 
                            <input type="email" class="form-control" placeholder="Email Address">
                        </div> 
                        <button type="submit" class="btn btn-success btn-flat mb-5 mt-10">Reset Password</button>  
                        <div class="register-link mt-15">
                            <p> 
                                Already have an account? <a href="index.php"> login here</a> 
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


</body>

</html>
