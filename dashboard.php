<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<!-- s:head -->
<title>Yureka</title>
<?php include "_head.php"; ?>
<!-- s:head -->

<body>

    <!-- Left Panel -->
    <?php include "_left-panel.php"; ?> 
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <?php include "_header.php"; ?> 
        <!-- Header-->  

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">Page</a></li>
                                    <li class="active"><a href="#">Page Detail</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .breadcrumbs -->


        <div class="content"> 
            <div class="container-fluid"> 
                        
                <!-- s:isi content -->
                    <!-- STATISTIC-->
                    <section class="statistic"> 
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="statistic__item shadows">
                                    <h2 class="number">90%</h2>
                                    <span class="desc">Kas Kecil Satu</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-account-o"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="statistic__item shadows">
                                    <h2 class="number">10%</h2>
                                    <span class="desc">Kas Kecil Dua</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-shopping-cart"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="statistic__item shadows">
                                    <h2 class="number">40%</h2>
                                    <span class="desc">Kas Kecil Tiga</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-calendar-note"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="statistic__item shadows">
                                    <h2 class="number">40%</h2>
                                    <span class="desc">Kas Kecil Empat</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-money"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3">
                                <div class="statistic__item shadows">
                                    <h2 class="number">20%</h2>
                                    <span class="desc">Kas Kecil Lima</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-account-o"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="statistic__item shadows">
                                    <h2 class="number">70%</h2>
                                    <span class="desc">Kas Kecil Enam</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-shopping-cart"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="statistic__item shadows">
                                    <h2 class="number">20%</h2>
                                    <span class="desc">Kas Kecil Tujuh</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-calendar-note"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="statistic__item shadows">
                                    <h2 class="number">90%</h2>
                                    <span class="desc">Kas Kecil Delapan</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-money"></i>
                                    </div>
                                </div>
                            </div>

                        </div> <!-- .row --> 
                    </section>
                    <!-- END STATISTIC--> 
                <!-- e:isi content -->  

                    
            </div> <!-- container-fluid -->
        </div> <!-- .content -->

       <?php include "_footer.php"; ?> 

    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <?php include "_js.php"; ?>
    

</body>

</html>
