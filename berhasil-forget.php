<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<!-- s:head -->
<title>Yureka - Register</title>
<?php include "_head.php"; ?>
<!-- s:head -->

<body class="bg-dark">

    <div class="d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo"> 
                    <img class="align-content" src="images/logo-eyureka.png" alt="eyureka"> 
                </div>
                <div class="login-form notifnya">
                    <div class="row">
                        <div class="col-12">
                            <div class="d-block text-center">
                                <i class="fa fa-gears"></i> 
                            </div> 
                            <p>
                                <b>You have requested to reset your password.</b> <br>
                                We cannot simple send your old password. A unique link to reset your password has been generated for you. To reset your password, click 
                                the following link and follow the instructions. 
                            </p>
                            <a href="index.php" class="btn btn-success">Reset Password</a> 
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


</body>

</html>
