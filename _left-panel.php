


    <aside id="left-panel" class="left-panel sidebar-shadow">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header shadows">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="dashboard.php"><img src="images/logo-eyureka.png" alt="yureka"></a>
                <a class="navbar-brand hidden" href="dashboard.php"><img src="images/ico-eyureka.png" alt="yureka"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">

                <div class="accountz">
                    <div class="imagez">
                        <img src="images/avatar/avatar-big-01.jpg" alt="Chelsea">
                    </div>
                    <h4 class="name">Chelsea Islan</h4>
                    <a href="index.php">Sign out</a>
                </div> 

                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="dashboard.php"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <li class="">
                        <a href="kas-kecil.php"> <i class="menu-icon fa fa-money"></i>Kas Kecil</a>
                    </li>
                    <li class="">
                        <a href="role.php"> <i class="menu-icon fa fa-stack-exchange"></i>Role</a>
                    </li>
                    <li class="">
                        <a href="permission.php"> <i class="menu-icon fa fa-exclamation-triangle"></i>Permission</a>
                    </li>
                    <li class="">
                        <a href="add-new-user.php"> <i class="menu-icon fa fa-male"></i>Add New User</a>
                    </li>
                    <h3 class="menu-title">UI elements</h3><!-- /.menu-title -->
                    <li class="">
                        <a href="add-new-form.php"> <i class="menu-icon fa fa-list-alt"></i>Add New Form</a>
                    </li> 
                </ul>

                <!-- <div class="logo-company">
                    <img src="images/data/logo-company.jpg">
                </div> -->
            </div><!-- /.navbar-collapse -->
            
        </nav>
    </aside><!-- /#left-panel --> 
     