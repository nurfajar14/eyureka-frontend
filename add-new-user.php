<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<!-- s:head -->
<title>Yureka -  Add New User</title>
<?php include "_head.php"; ?>
<!-- s:head -->

<body>

    <!-- Left Panel -->
    <?php include "_left-panel.php"; ?> 
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <?php include "_header.php"; ?> 
        <!-- Header-->  

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Add New User</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">Form</a></li>
                                    <li class="active"><a href="#">Add New User</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .breadcrumbs -->


        <div class="content"> 
            <div class="container-fluid"> 
                        
                <!-- s:isi content --> 
                <div class="row">

                            <div class="col">
                                <div class="card shadows">
                                    <div class="card-header">Form</div>
                                    <div class="card-body card-block">
                                        <form action="" method="post" class=""> 

                                            <div class="form-group"> 
                                                 <span class="control-label mb-1">Username</span> 
                                                 <input type="text" class="form-control">   
                                            </div>  

                                            <div class="form-group">  
                                                <span class="control-label mb-1">Pangkat User</span> 
                                                <select name="select" id="select" class="form-control">
                                                    <option value="0">Please select</option>
                                                    <option value="1">Pangkat 1</option>
                                                    <option value="2">Pangkat 2</option>
                                                    <option value="3">Pangkat 3</option>
                                                </select>   
                                            </div>  

                                            <div class="form-group"> 
                                                    <span class="control-label mb-1">Email</span> 
                                                    <input type="email" id="email3" name="email3" class="form-control">  
                                            </div>  

                                            <div class="form-group"> 
                                                    <span class="control-label mb-1">No HP</span> 
                                                    <input type="text" id="email3" name="email3" class="form-control">  
                                            </div>   

                                            <div class="form-group"> 
                                                    <span class="control-label mb-1">Password</span> 
                                                    <input type="password" id="password3" name="password3" class="form-control"> 
                                            </div>   

                                            <div class="form-group"> 
                                                    <span class="control-label mb-1">Confirm Password</span> 
                                                    <input type="password" id="password3" name="password3" class="form-control"> 
                                            </div>   
                                            
                                            

                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </div> <!-- col -->

                        </div> <!-- row -->
                <!-- e:isi content -->   
                    
            </div> <!-- container-fluid -->
        </div> <!-- .content -->

       <?php include "_footer.php"; ?> 

    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <?php include "_js.php"; ?>
    

</body>

</html>
