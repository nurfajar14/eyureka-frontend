<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<!-- s:head -->
<title>Yureka - Register</title>
<?php include "_head.php"; ?>
<!-- s:head -->

<body class="bg-dark">

    <div class="d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo"> 
                    <img class="align-content" src="images/logo-eyureka.png" alt="eyureka"> 
                </div>
                <div class="login-form notifnya">
                    <div class="row text-center">
                        <div class="col-12"> 
                            <i class="fa fa-check"></i>
                            <h2 class="judul1">Success</h2> 
                            <p>
                                Dear, <strong>Chelsea Islan</strong>
                                thank you for signing up. We have sent you an email "chelsea@gmail.com" with your details
                                Please go to your above email now and login.
                            </p>
                            <a href="index.php" class="btn btn-success">Sign in</a> 
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


</body>

</html>
