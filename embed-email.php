<html lang="en">
<head>
    <title>Yureka</title> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="description" content="yureka">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
</head>

<style>

.body {
    font-family: tahoma;
    font-size: 14px;
    border: 1px solid #d4d4d4;
    margin: 0 auto;
    padding: 50px;
    background-color: #fff;
    border-radius: 5px;
    text-align:center;
    width:50%;
    color: #585858;
}

h3 {
    font-weight: 600;
    font-size: 20px;
    MARGIN: 0 0 20px 0;
}

.footer {
    border-top: 1px solid #d2d2d2;
    padding: 20px 0 0 0;
    margin: 20px 0 0 0;
    font-size:12px;
}

.btn {
    display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    padding: 0.7rem 1.4rem;
    font-size: 0.9rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;

    color: #fff;
    background-color: #28a745;
    border-color: #28a745;

    text-decoration:none;
    text-transform: uppercase; 

    margin: 20px 0 15px 0;
}

.logo {
    font-style: italic;
    margin: 30px auto 0 auto;
    font-size: 80px;
    color: #fff;
    text-align: center;
    line-height: 1;
    font-family: sans-serif;
    font-weight: bolder;
    letter-spacing: -12px;
}
    
</style>

<body style="background-color: #343a40;"> 
    <DIV class="logo">EYUREKA</DIV>
    <div class="body"> 
        <h3>Password Reset</h3>           
        <p>
            if you have lost your password or wish to reset it, use the link below to get started.
        </p>
        <a href="#" class="btn">Reset Password</a>
        <p class="footer">
            if you did not request a password reset, you can safely ignore this email. 
            Only a person with access to your email can reset your account password.
        </p>
    </div> 
</body>

</html>